---
title: "The Misapplied and Misunderstood: Two-pizza team rule"
publishdate: 2022-04-14
canonicalUrl: https://polarsquad.com/blog/missapplied-and-misunderstood-2pt-rule
tags: [devops]
---

When it comes to software development, there are two forces at play. While productivity has taken huge leaps forward, the field as a whole has gotten more and more complex year by year. The diversity of software, variety of platforms and the number of disciplines involved are all increasing rapidly. Web development, mobile development, infrastructure engineering, data science, and others are faced with more complex tasks while expected to handle more tools and platforms.

The amount of knowledge is overwhelming and often there’s little time to invest in learning new things. With big players working on so many cool, advanced, and successful things, it’s easy for FOMO (fear of missing out) to creep in.

To keep up with the fashion, there’s a trend of favoring quick, catchy answers without taking much time to think how well they apply to the situation at hand. As these practices spread and less time is spent on thinking about them, they gradually become “best practices”, dogmas that are no longer questioned.

Even worse is when a practice is misunderstood (accidentally or maliciously) to mean something different than what it was originally. For example, using microservices to try to scale and parallelize software development when there are only a handful of people and a small amount of features involved. This usually happens when people forget the original context due to long-term misapplication of the practice. From what I’ve seen as a consultant working on several customer projects, blindly following ideas is always more likely to lead to disaster than success.

In this series of blog posts, I would like to promote the idea of deconstructing the original ideas behind the practices we see being used, learning how they are misinterpreted, and learning how and when we should apply them. Let’s start this series with a topic that’ll make you wish it was lunchtime already.

## Two-pizza team rule

> "We try to create teams that are no larger than can be fed by two pizzas," said Bezos. "We call that the two-pizza team rule."

The two-pizza team rule (abbreviated as 2PT) is an approach to improving collaboration within development teams. It originates from Jeff Bezos’ early years at Amazon. According to [AWS’s documentation](https://docs.aws.amazon.com/whitepapers/latest/introduction-devops-aws/two-pizza-teams.html), it’s still in use.

The core idea of 2PT is that small teams are better at collaborating than large teams, which usually means they’re better at delivering value and responding to change, i.e. they’re more productive.

This makes sense – work overhead tends to grow as more people need to be kept in sync. As team sizes increase, more time is spent on facilitating the work, viewpoints, and decisions for all team members. More things are worked on concurrently, which means there’s more effort required in designing the work so that the team members don’t end up blocking each other’s work.

On the other hand, based on my experience, a small team can get away with less communication as there’s less concurrent work in progress and fewer inputs to consider. With fewer people, it’s also easier to get to know every team member, which means there’s more chance for the team to understand how to better work together.

What about the size of the pizza? The 2PT rule suggests that the maximum size is dictated by the amount of pizza and how much of it the team members need to be fed.

In Finland, a medium-sized pizza is about 30 cm in diameter. To me, it suggests that a single two-pizza team would optimally consist of 2 people, but it could be pushed to 3-4 people if the people are not starving (or drunk). More than 4 people would be way too many. This makes a lot of sense to me. I think team sizes ranging from 2-4 people have worked the best for me in the past.

![Two average-size pizzas in Finland](images/2-pizzas.jpg)

However, when you check out what other people think is the appropriate team size, they mention that the teams should have 6-10 members. *What?!* How on earth can you support that many people with only two pizzas? How large are these pizzas? Who decides the toppings? What if somebody chose pineapple?!

## Forget the pizza!

All jokes aside, the conversation on 2PT quickly turns into debate over what is the optimal team size for maximizing the productivity of the team. The optimal team size is very subjective and depends on several factors. For example:

* Is the team working on something that requires people with multiple backgrounds such as software developers, cloud engineers, data scientists, UI designers, etc?
* How much of the work can the team get as a service from another team or a 3rd party provider instead of doing the work themselves?
* Is the team able to both support what they built and develop things further with the current headcount or are they stretched too thin?
* Are the members in the team used to working with a particular team size?

There are many more questions to answer, and it’s not uncommon for the answers to differ between teams within the same organization.

What I’ve found successful is to start with a small team and then iterate, learn, and adapt as needed. Starting with a smaller team means that you can scale out when more hands are required. If you start with a bigger team and realize that there’s not enough work for everyone, it can be awkward to restructure and reassign members afterward.

## There’s more

Small 2PT-style teams are awesome, but they mean absolutely nothing if you don’t support them. In many of the customer cases I’ve been involved in, organizations have restructured their teams into smaller teams to fit 2PT, but not set up the structures for them to function properly. What the teams usually lack is autonomy and accountability.

Without autonomy, the productivity goals of 2PT evaporate. What carries the idea behind 2PT is that the teams remain in charge of the delivery of their work from start to finish, from idea to production. This often requires a big [mindset change from the entire organization](https://polarsquad.com/blog/accepting-the-good-and-bad-of-team-autonomy). Here’s how lack of autonomy drives productivity into the ground:

* Decisions from the higher-ups in the organization automatically override the decisions made in teams – without involving the teams in the discussions.
* Dependencies between teams block teams from carrying out their tasks across the finish line.

With autonomy comes responsibility; teams must be accountable for what they do. Without it, teams may lose focus and end up not delivering the value they’re supposed to. I’ve seen both teams and entire organizations fail due to lack of support from both sides:

* There’s not enough motivation for the team members to take responsibility beyond a partial scope. For example, a development team might be willing to focus on writing code but are not interested in deploying it to production.
* Due to the way work is organized, teams may be unable to take control of the workflow they’re responsible for. For example, a development team is expected and willing to support their application in production but they’re not given enough resources or tools to do so.
* The wider organization fails to take into teams’ ability to meet the expectations. These issues are not followed up properly, which eats away teams’ motivation. For example, the leadership accepts and delegates a feature request (with a deadline) from the customer to a development team without first checking with the team.

Amazon’s solution has been to use [a fitness function per team](https://jasoncrawford.org/two-pizza-teams) to provide focus and accountability for teams. The idea is that each team agrees on a key business metric with the wider organization to grow. However, at the same time, some teams find that distilling the team to a single metric is [uncomfortable and demotivating in itself](https://www.techinasia.com/jeff-bezoss-muchdiscussed-twopizza-rule-failure).

## Conclusion

In my opinion, culture and practices can’t and shouldn't be just outright copied. Instead, I urge everyone to stop worrying about missing out, really dig behind the reasons why the practices exist and think about whether they apply in their context. In this article, I’ve used the two-pizza team rule as an example of how people interpret it and what it means and requires from an organization before they can apply it. 2PT is best taken as a starting point: it absolutely requires careful thought and regurgitation before it’s ready to be unleashed within your organization.

Thanks for reading!
