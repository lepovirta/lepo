---
title: Lepo
aliases:
  - /about.html
  - /code.html
  - /team/jkpl/
---

<div class="hero">

<dfn title="finnish: rest, repose">Lepo</dfn> is a space for sharing and collaborating on ideas, tools, and knowledge to help software developers, infrastructure engineers, and other IT professionals of all skill levels.

</div>

<!--more-->

Hi! I'm Jaakko Pallari, the author and maintainer of this site and the related software projects.
I write about [everything in software I've learned and built](#posts), and share [the software solutions that I've found useful](#projects).

At work, I help bring people in the IT industry together to deliver exactly what is needed in a rapid, reliable, and stress-free way with the help of software, cloud, and software delivery and maintenance practices.
Most of my public code work is visible in [GitLab](https://gitlab.com/jkpl) and [GitHub](https://github.com/jkpl).

The best you can get in touch with me via email jkpl **ät** lepovirta **döt** org.

## Projects

There are a few software and infrastructure projects maintained under the Lepo group.
All of the software is [free and open source](https://en.wikipedia.org/wiki/Free_and_open-source_software).
The code can be found from [GitLab](https://gitlab.com/lepovirta) with most of the projects mirrored to [GitHub](https://github.com/lepovirta).

None of the projects have any schedules, and they will be worked on whenever anyone feels like working on them.
If you wish to contribute to the projects, please favor the GitLab repos over the GitHub repos.
